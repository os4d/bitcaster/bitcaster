# bitcaster


[![pipeline status](https://gitlab.com/os4d/bitcaster/bitcaster/badges/develop/pipeline.svg)](https://gitlab.com/os4d/bitcaster/bitcaster/-/commits/develop)
 [![coverage report](https://gitlab.com/os4d/bitcaster/bitcaster/badges/develop/coverage.svg)](https://gitlab.com/os4d/bitcaster/bitcaster/-/commits/develop)
 [![Latest Release](https://gitlab.com/os4d/bitcaster/bitcaster/-/badges/release.svg)](https://gitlab.com/os4d/bitcaster/bitcaster/-/releases)
 [![codecov-badge]][codecov-link]

Bitcaster is a system-to-user signal-to-message notification system.

Bitcaster will receive signals from any of your applications/systems using a simple RESTful API and will convert them in messages to be distributed to you users via a plethora of channels.

Messages content is customised at user/receiver level using a flexible template system.

Your user will be empowered with an easy to use console to choose how to receive the messages configured in Bitcaster.



[codecov-badge]: https://codecov.io/gh/os4d:bitcaster/bitcaster/branch/develop/graph/badge.svg
[codecov-link]: https://app.codecov.io/gl/os4d:bitcaster/bitcaster


# Resources

- [Bug Tracker](https://gitlab.com/os4d/bitcaster/bitcaster/-/issues)
- [Code](https://gitlab.com/os4d/bitcaster/bitcaster)
- [Transifex](https://explore.transifex.com/os4d/bitcaster/) (Translate Bitcaster\!)
