# Welcome to Bitcaster Documentation

See [Glossary](./glossary/index.md) for a list of common Bitcaster terminology

See [Useful plugins](https://github.com/mkdocs/catalog#-git-repos--info) for a list of possible mkdocs plugins to install
