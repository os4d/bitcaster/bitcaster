<!-- 
This is a comment, it will not be rendered by the Markdown engine. You can use it to provide instructions how to fill in the template.
--> 

### Summary

<!-- Summarize the bug encountered concisely. -->

### Steps to reproduce

<!-- Describe how one can reproduce the issue - this is very important. -->

### Output of checks

<!-- If you are reporting a bug on GitLab.com, write: This bug happens on GitLab.com -->

#### Results of GitLab environment info

<!--  Input any relevant GitLab environment information if needed. -->

<details>
<summary>Expand for output related to app info</summary>

<pre>

(Paste the version details of your app here)

</pre>
</details>

### Possible fixes

<!-- If you can, link to the line of code and suggest actions. →

## Maintainer tasks

- [ ] Problem reproduced
- [ ] Weight added
- [ ] Fix in test
- [ ] Docs update needed

/label ~"type::bug"
